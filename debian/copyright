Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://www.mplayerhq.hu/dload.html

Files: *
Copyright:
 2005-2006 Franciszek Wilamowski <higgs@hoth.amu.edu.pl>
License: BSD-3-Clause
Comment: The upstream tar.bz2 does not contain a license statement. The license
 was confirmed by a statement provided by the author.
 .
 On Thu, 30 Jul 2015 there was an attempt to contact the author via the MPlayer
 IRC channel, their mailing list and emails to the previous Debian maintainers
 to see if they had a copy of that statement. In short, it seems that even the
 MPlayer developers have tried to contact him and others skin developers and
 have failed. He is impressively off the grid and nothing can be found about
 him online. Unless the old maintainers saved the statement from 2006 the only
 reference is the copyright file from the 1.6-2 package. That file was updated
 to the DEP-5 format and no other change was made.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. Neither the name of the project nor the names of its contributors may be
 used to endorse or promote products derived from this software without specific
 prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
